<?php
class Session{
	function __construct() {
		session_start();
	}

	function startSession(){
		session_start();
	}

	function adminLoginStatus(){
		if(! isset($_SESSION['admin'])){
			return false;
		}else{
			return true;
		}
	}

	function userLoginStatus(){
		if(! isset($_SESSION['user'])){
			return false;
		}else{
			return true;
		}
	}

	function userLogin($id, $name, $email){
		if(isset($name) && isset($id) && isset($email)){
			$_SESSION['user'] = array('name' => $name, 'id' => $id, 'email' => $email);
			return true;
		}else{
			return false;
		}
	}

	function cleanMessage(){
		//清除錯誤訊息
		unset($_SESSION['error']);
		//清除成功訊息
		unset($_SESSION['success']);
		//清除表單輸入資料
		unset($_SESSION['form']);
	}
}
