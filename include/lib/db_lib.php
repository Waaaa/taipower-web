<?php
class DB{
	function __construct(){
		try{
			$this->db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8", DB_USER, DB_PASSWD);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$this->connectionSataus = 1;
		}catch(PDOException $e){
			$this->connectionSataus = 0;
			exit;
		}
	}

	function status(){
		echo $this->connectionSataus;
	}

	//驗證管理者登入
	function adminLogin($acc, $password){
		$sql = "SELECT * FROM admin WHERE acc = ?";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array($acc));
		$admin = $stmt->fetchAll();
		if( count($admin) == 1 && $password == $admin[0]['password'] ){
			$_SESSION['admin'] = $admin[0]['acc'];
			return true;
		}else{
			return false;
		}
	}

	//變更密碼
	function changePassword($acc, $newPassword){
		$sql = "UPDATE admin SET password = ? WHERE acc = ?";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array($newPassword, $acc));
		if($res){
			return true;
		}else{
			return false;
		}
	}

	//取得資料總筆數
	function getRowCount($table){
		$sql = "SELECT count(*) FROM ".$table;
		$res = $this->db->query($sql);
		return $res->fetchcolumn();
	}

	//判斷特定id是否存在資料
	function issetItem($table, $id){
		if(! is_numeric($id)){
			return false;
		}
		$sql = "SELECT count(*) FROM $table WHERE id = ?";
		//$res = $this->db->query($sql);
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array($id));
		$count = $stmt->fetchColumn();
		if($count > 0 && $res){
			return true;
		}else{
			return false;
		}
	}

	//刪除特定id的資料
	function deleteItem($table, $id){
		if(! is_numeric($id)){
			return false;
		}
		$sql = "DELETE FROM $table WHERE id = ?";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array($id));
		if($res){
			return true;
		}else{
			return false;
		}
	}

	//取得特定id的資料
	function getItem($table, $id){
		if(! is_numeric($id)){
			return false;
		}
		$sql = "SELECT * FROM $table WHERE id = ?";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array($id));
		if($res){
			return $stmt->fetch();
		}else{
			return false;
		}
	}

	function getLastUpdate($table){
		$sql = "SELECT * FROM $table ORDER BY created_at DESC LIMIT 1";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute();
		if($res){
			$data = $stmt->fetchAll();
			if(count($data) > 0){
				return $data[0]['created_at'];
			}else{
				return "";
			}
			
		}else{
			return "";
		}
	}

	//locations
	function getLocations(){
		$sql = "SELECT * FROM locations ORDER BY created_at DESC";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute();
		return $stmt->fetchAll();
	}

	function getLocationsByID($id){
		$sql = "SELECT * FROM locations WHERE device_id = ? ORDER BY created_at DESC";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array($id));
		return $stmt->fetchAll();
	}

	

	
}