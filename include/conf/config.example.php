<?php
define('DB_HOST', 'YOUR_DB_HOST');
define('DB_NAME', 'YOUR_DB_NAME');
define('DB_USER', 'YOUR_DB_USERNAME');
define('DB_PASSWD', "YOUR_DB_PASSWORD");

set_include_path('YOUR_INCLUDE_PATH');
date_default_timezone_set("Asia/Taipei");

define('CONFIG_URL',"conf/");
define('LIB_URL',"lib/");

define('HOST',"YOUR_HOST");
define('IMG_DIR', HOST."taipower/asset/images/");
define('CSS_DIR', HOST."taipower/asset/css/");
define('JS_DIR', HOST."taipower/asset/js/");
define('INDEX_URL', HOST."taipower");
define('ADMIN_URL', HOST."taipower/admin/");
define('LOGIN_URL', HOST."taipower/admin/login.php");
define('LOGOUT_URL', HOST."taipower/admin/auth.php?action=logout");
define('ANDROID_APK_URL', HOST."taipower/asset/apk/android.apk");
//維護模式
define('DEBUG', false);
define('DEBUG_TITLE', '網站維護中');
define('DEBUG_MSG', '網站維護中，請稍後再嘗試連線，謝謝!');

include_once LIB_URL."session_lib.php";
include_once LIB_URL."db_lib.php";
$db = new DB();
$session = new Session();

header("Content-Type:text/html; charset=utf-8");

