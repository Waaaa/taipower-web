<?php

include_once "../../include/conf/config.php";

if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['action'])){
	
	$action = $_POST['action'];
	$respone = ['status' => 0];

	header('content-type: application/json');
	header('Access-Control-Allow-Origin: *'); 

	switch ($action) {
		case 'insert':
			
			//接收資料		
			$lng = $_POST['lng'];
			$lat = $_POST['lat'];
			$device_id = $_POST['device_id'];

			//目前時間
			$d = new DateTime();
			$time = $d->format('y-m-d H:i:s');

			$sql = "INSERT INTO locations VALUES('', ?, ?, ?, ?)";
			$stmt = $db->db->prepare($sql);
			$res = $stmt->execute(array($lng, $lat, $time, $device_id));
			if($res){
				$respone = ['status' => 1];
			}else{
				$respone = ['status' => 0];
			}
			

			break;
		
		default:
			# code...
			break;
	}
	echo json_encode($respone);
	exit;
}


echo json_encode(['status' => 0]);
exit;

?>