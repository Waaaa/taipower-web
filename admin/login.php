<?php
include_once "../include/conf/config.php";
$cssPath = CSS_DIR.'login.css';
//表單傳送路徑
$formPath = ADMIN_URL."auth.php?action=login";

if($session->adminLoginStatus()){
    header("location: ".INDEX_URL);
    exit;
}
?>
<html>
<head>
	<title>Taipower</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="<?=$cssPath?>">
</head>
<body>
<div id="loginPage" class="jumbotron jumbotron-fluid mb-0">
  <div class="container ">
    <form action="<?=$formPath?>" method="POST" id="loginBox" class="col-lg-8 offset-lg-2 col-12 offset-0 p-4">
    	<center><h4 class="font-weight-bold ">Taipower</h4></center>
    	<hr>    	
    	<?php
    	if( @$_SESSION['error']['action'] == "login"){
    		echo '<div class="alert alert-danger" role="alert">'.$_SESSION['error']['message'].'</div>';
    	}
    	?>
		<div class="form-inline mb-4">
    		<label class="col-4 text-center"><h5 class="font-weight-bold">帳號</h5></label>
    		<input type="text" name="acc" class="form-control col-6">
    	</div>
		<div class="form-inline mb-4">
    		<label class="col-4 text-center"><h5 class="font-weight-bold">密碼</h5></label>
    		<input type="password" name="password" class="form-control col-6">
    	</div>

    	<div class="form-inline">
    		<button type="submit" class="btn btn-primary col-2 offset-5">登入</button>
    	</div>
    </form>
  </div>
</div>
<?php
$session->cleanMessage();
?>
</body>
</html>
