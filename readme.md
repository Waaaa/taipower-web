# 定位示範系統伺服器端
**2018年11月開發**  

## 系統說明
**檢視定位資訊**  
網站首頁將會列出目前資料庫所儲存的所有定位資訊，以定位時間遞減排序，並顯示以下資料：
* 裝置ID
* 經度 (Lat)
* 緯度 (Lng)
* 建立時間
並提供開啟地圖之連結，點擊後將會開新視窗，以Google地圖服務顯示使用者定位資訊。

**搜尋定位資訊**  
使用者可以藉由裝置ID來搜尋特定裝置的定位資訊。

## API功能
| 功能      | URL                    | Method | Request | Respone             |
| ------- | ---------------------- | ------ | ------- | ------------------- |
| 新增定位資訊   | /taipower/api/location/action.php               | POST   | 請求的服務、裝置ID與定位資訊   | 回傳請求完成狀態(1: 成功、0: 失敗) |

新增定位資訊請求示範：
```JSON
POST /taipower/api/location/action.php

{
	"action": "insert",
	"device_id": {device_id},
	"lng": {lng},
	"lat": {lat}
}
```

新增定位資訊回應示範：
```JSON
{
	"status": "1"
}
```

## 資料庫儲存的資料
* GPS定位
* 使用者的裝置之ID

## 資料庫設計
```SQL
CREATE DATEBASE IF NOT EXISTS `taipower`;

CREATE TABLE IF NOT EXISTS `admin`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`acc` varchar(10) NOT NULL,
	`password` varchar(50) NOT NULL,
	PRIMARY KEY(`id`),
	UNIQUE(`acc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf-8 COLLATE=utf-8_unicode_ci COMMENT='管理者資料表';

CREATE TABLE IF NOT EXISTS `locations`(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`lng` double NOT NULL,
	`lag` double NOT NULL,
	`created_at` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`device_id` varchar(50) NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf-8 COLLATE=utf-8_unicode_ci COMMENT='位置資料表';
```

## 開發工具與設備環境
* PHP 7.3.5
* Apache 2.4
* MariaDB 10.1.40