<?php
include_once "include/conf/config.php";

//判斷是否在維護模式
if(DEBUG){
	if(! $session->adminLoginStatus()){
	    header("location: ".LOGIN_URL);
	    exit;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Taipower</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?=CSS_DIR?>index.css">
</head>
<body>
<main role="main">
	<nav class="navbar navbar-expand-md navbar-light bg-light">
		<div class="container">
			<a href="<?=INDEX_URL?>"><h1 class="navbar-brand font-weight-bold">Taipower</h1></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul class="navbar-nav mr-auto">
			    </ul>
			    <ul class="navbar-nav my-2 my-lg-0">
					<li class="nav-item">
						<a class="nav-link" href="<?=ANDROID_APK_URL?>" download>Download APP</a>
					</li>
					<li class="nav-item d-none d-md-block">
						<a class="nav-link">|</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=LOGOUT_URL?>">Sign out</a>
					</li>
			    </ul>
			  </div>
		</div>

	</nav>
	<section class="jumbotron d-flex justify-content-center align-items-center">
	    <div class="container">
		    <div id="jumbotron-content-section" class="inner">
		    	<form class="row" method="GET" action="<?=INDEX_URL?>">
		    		<div id="search-section" class="col-12 p-4 bg-dark form-inline">
		    			<input type="text" name="id" class="form-control col-11" placeholder="請輸入您的裝置 ID">	
		    			<button type="submit" class="btn btn-dark col-1"><i class="fas fa-search"></i></button>
		    		</div>
		    	</form>	
		</div>
	  </div>
	</section>
	<section class="container">
		<div class="row">
			<table id="content-table" class="table col-12">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">裝置 ID</th>
			      <th scope="col">經度 (Lat)</th>
			      <th scope="col">緯度 (Lng)</th>
			      <th scope="col">建立時間</th>
			      <th scope="col"></th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php
			  	if(isset($_GET['id'])){
			  		$data = $db->getLocationsByID($_GET['id']);
			  	}else{
			  		$data = $db->getLocations();
			  	}

			  	for($i = 0 ; $i < count($data) ; $i++){
			  		$number = $i + 1;
			  	?>
			    <tr>
			      <td scope="row"><?=$number?></td>
			      <td><?=$data[$i]['device_id']?></td>
			      <td><?=$data[$i]['lat']?></td>
			      <td><?=$data[$i]['lng']?></td>
			      <td><?=$data[$i]['created_at']?></td>
			      <td class="text-center"><a target="_blank" title="開啟地圖 (開新頁面)" href="http://maps.google.com/maps?q=<?=$data[$i]['lat']?>,<?=$data[$i]['lng']?>" class="btn btn-info"><i class="fas fa-map-marker-alt"></i></a></td>
			    </tr>
			    <?php } ?>
			  </tbody>
			</table>
		</div>	
		<div class="text-center mt-5 text-secondary">
			<?php
			if(count($data) > 0){
				echo "共 ".$db->getRowCount('locations')." 筆資料，最後更新於 ".$db->getLastUpdate('locations');
			}
			?>
		</div>
	</section>

</main>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>